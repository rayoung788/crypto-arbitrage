const express = require('express')
var cors = require('cors')
const env = require('./env')
const path = require('path')
const app = express()
const ccxt = require('ccxt')

// cached exchange data
const cache = {}

app.use(cors())
// point to the static files folder (built client files)
app.use(express.static('static'))

app.get('/exchanges', (req, res) => {
  res.json(ccxt.exchanges)
})

// check to see if exchange data is already loaded
const load = async (req, res, next) => {
  const exchange = req.params.exchange
  if (!cache[exchange]) {
    cache[exchange] = new ccxt[exchange]()
    try {
      await cache[exchange].loadMarkets()
    } catch (err) {
      cache[exchange].symbols = {error: true, message: err.message}
    }
  }
  next()
}

// get all supported markets for a particular exchange
app.get('/markets/:exchange', load, async (req, res) => {
  res.json(cache[req.params.exchange].markets)
})

// get all supported symbols for a particular exchange
app.get('/symbols/:exchange', load, async (req, res) => {
  res.json(cache[req.params.exchange].symbols)
})

// get base/quote pair orderbook at a particular exchange
// ex: /orderbook/binance?base=ETH&quote=BTC
app.get('/orderbook/:exchange', load, async (req, res) => {
  const pair = `${req.query.base}/${req.query.quote}`
  res.json(await cache[req.params.exchange].fetchOrderBook(pair))
})

// client code entrypoint
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'))
})

app.listen(3000, () =>
  console.log(`Example app listening on http://${env.host}:${env.port}/`)
)
