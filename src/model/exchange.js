import api from '@/model/api'
class Exchange {
  constructor({
    pairs,
    name
  }) {
    // all symbol pairs to iterate through
    this.pairs = pairs
    // name of the exchange
    this.name = name
    // storage for current best bid & ask prices
    this.data = pairs.reduce((res, pair) => {
      res[pair] = {}
      return res
    }, {})
    // iterator index for which pair is next to be queried
    this.idx = 0
    // interval to query the current pair accoding to the index
    this.interval = setInterval(async () => {
      // the current pair to query
      const pair = this.pairs[this.idx]
      // get the orderbook for the current pair
      const orderbook = await api.orderbook(this.name, pair)
      // record the best ask & bid prices
      this.data[pair] = {
        timestamp: new Date().toLocaleString(),
        bid: parseFloat(orderbook.bids.length ? orderbook.bids[0][0] : undefined),
        ask: parseFloat(orderbook.asks.length ? orderbook.asks[0][0] : undefined)
      }
      // cycle over to the next pair
      this.idx = (this.idx + 1) % this.pairs.length
    }, 5000)
  }
}

export default Exchange
