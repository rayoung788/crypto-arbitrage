import axios from 'axios'
const host = 'http://localhost:3000/' // http://localhost:3000
// get all exchanges server-side endpoint interface
const exchanges = async () => {
  const result = await axios.get(`${host}exchanges`)
  return result.data
}
// get symbols for exchange server-side endpoint interface
const symbols = async exchange => {
  const result = await axios.get(`${host}symbols/${exchange}`)
  return result.data
}
// get pair orderbook for exchange server-side endpoint interface
const orderbook = async (exchange, pair) => {
  const [base, quote] = pair.split('/')
  const result = await axios.get(`${host}orderbook/${exchange}?base=${base}&quote=${quote}`)
  return result.data
}

export default {
  exchanges,
  orderbook,
  symbols
}
